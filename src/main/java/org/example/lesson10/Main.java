package org.example.lesson10;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class Main {
    private static final Random random = new Random();

    public static void main(String[] args) {
        log.info("Duplicated removed: {}",
                removeDuplicates(Stream.of(1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3).collect(Collectors.toList())));
        testCollections();
        testGame();
        log.info("{}", arrayToMap(new Integer[]{1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3}));
    }

    /**
     * Написать метод removeDuplicates, который на входе получает коллекцию объектов,
     * а возвращает коллекцию уже без дубликатов.
     */

    public static List<Object> removeDuplicates(List<Object> collection) {
        return collection.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public static Set<Object> removeDuplicates(Set<Object> collection) {
        return collection;
    }

    public static Queue<Object> removeDuplicates(Queue<Object> collection) {
        return collection.stream()
                .distinct()
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Напишите метод, который добавляет 1млн элементов в ArrayList и LinkedList.
     * Напишите метод, который выбирает из заполненного списка элемент наугад 100000 раз.
     * Замерьте время, которое потрачено на это. Сравните результаты, предположите, почему они именно такие.
     */

    public static void testCollections() {
        final int LIMIT = 50000;
        ArrayList<Integer> integerArrayList = new ArrayList<>();
        LinkedList<Integer> integerLinkedList = new LinkedList<>();
        Stream.of(integerArrayList, integerLinkedList).forEach(list -> {
            LocalDateTime startTime = LocalDateTime.now();
            for (int i = 0; i < LIMIT; i++) {
                list.add(i);
            }
            LocalDateTime now = LocalDateTime.now();
            log.info("На заполнение {} ушло {} мс.", list.getClass().getName(), startTime.until(now, ChronoUnit.MILLIS));
            startTime = LocalDateTime.now();
            for (int i = 0; i < LIMIT; i++) {
                // просто берем вникуда, нам не надо сохранять это значение
                list.get(random.nextInt(LIMIT));
            }
            now = LocalDateTime.now();
            log.info("На взятие элементов из {} ушло {} мс.", list.getClass().getName(), startTime.until(now, ChronoUnit.MILLIS));
        });
        log.info("Очевидно, что взять элемент по номеру из LinkedList гораздо дороже, чем из ArrayList");
        log.info("в силу особенностей их реализации");
    }


    /**
     * Создайте Map, в котором для каждого пользователя хранится количество очков,
     * заработанных в какой-то игре (Map<User, Integer>)
     * Напишите программу, которая считывает с консоли имя и показывает, сколько
     * очков у такого пользователя. Сами данные можно добавить в Map при создании или сгенерировать случайно.
     */

    public static void testGame() {
        Map<User, Integer> points = new HashMap<>();
        points.put(new User("Vasya"), 3);
        points.put(new User("Petya"), 4);
        points.put(new User("Masha"), 5);
        Scanner scanner = new Scanner(System.in);

        log.info("Введи имя игрока для поиска: ");
        String name = scanner.next();
        User user = points.keySet().stream()
                .filter(u -> name.equals(u.getName()))
                .findAny()
                .orElse(null);
        if (user != null) {
            log.info("User {} have {} points!", user.getName(), points.get(user));
        } else {
            log.info("I dunno such a user!");
        }
    }

    /**
     * Метод получает на вход массив элементов типа К.
     * Вернуть нужно объект Map<K, Integer>, где K — Значение из массива, а Integer -
     * количество вхождений в массив:
     *
     * <K> Map<K, Integer> arrayToMap(K[] ks);
     */
    public static <K> Map<K, Integer> arrayToMap(K[] ks) {
        Map<K, Integer> result = new HashMap<>();
        Arrays.stream(ks)
                .distinct()
                .forEach(k -> result.put(k, getCount(ks, k)));
        return result;
    }

    private static <K> int getCount(K[] ks, K obj) {
        return (int) Arrays.stream(ks)
                .filter(obj::equals)
                .count();
    }
}