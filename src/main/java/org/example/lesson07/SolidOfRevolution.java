package org.example.lesson07;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
public abstract class SolidOfRevolution extends Shape {

    private Double radius;

    public SolidOfRevolution(Double volume, Double radius) {
        super(volume);
        this.radius = radius;
    }
}
