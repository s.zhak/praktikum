package org.example.lesson07;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
public class Pyramid extends Shape {

    private Double s;
    private Double h;

    public Pyramid(Double s, Double h) {
        super(s * h / 3);
        this.s = s;
        this.h = h;
    }
}
