package org.example.lesson07;

import lombok.extern.slf4j.Slf4j;

import java.util.stream.Stream;

@Slf4j
public class Main {
    public static void main(String[] args) {
        do1();
        do2();
    }

    private static void do1() {
        HeavyTruck truck = new HeavyTruck(10000, "ЗИМ", '5', 120.5F, 18, 25000);
        truck.outPut();
    }

    private static void do2() {
        Box box = new Box(300d);
        Ball ball = new Ball(10d);
        Pyramid pyramid = new Pyramid(10d, 20d);
        Cylinder cylinder = new Cylinder(10d, 20d);

        Stream.of(ball, pyramid, cylinder)
                .map(box::add)
                .forEach(result -> log.info("Результат добавления: {}, сейчас коробка: {}, коробка заполнена на {} кг.", result, box, box.getCurrentVolume()));

    }
}