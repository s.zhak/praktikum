package org.example.lesson07;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@ToString
@Slf4j
public abstract class Shape {

    private Double volume;

    public Shape(Double volume) {
        log.info("Initializing {} with volume {}", this.getClass().getName(), volume);
        this.volume = volume;
    }

}
