package org.example.lesson07;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
public class Cylinder extends SolidOfRevolution {

    private Double height;

    public Cylinder(Double radius, Double height) {
        super(Math.PI * Math.pow(radius, 2d), radius);
        this.height = height;
    }
}
