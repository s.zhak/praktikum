package org.example.lesson07;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
public class Box extends Shape {

    private List<Shape> shapes;

    public Box(Double volume) {
        super(volume);
        this.shapes = new ArrayList<>();
    }

    public Boolean add(Shape shape) {
        if (getCurrentVolume() + shape.getVolume() > this.getVolume()) {
            return Boolean.FALSE;
        } else {
            this.shapes.add(shape);
            return Boolean.TRUE;
        }
    }

    public Double getCurrentVolume() {
        return shapes.stream()
                .map(Shape::getVolume)
                .reduce(0D, Double::sum);
    }
}
