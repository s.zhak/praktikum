package org.example.lesson07;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
public class Ball extends SolidOfRevolution {

    public Ball(Double radius) {
        super(4 * Math.PI * Math.pow(radius, 3d) / 3, radius);
    }

}
