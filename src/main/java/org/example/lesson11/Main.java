package org.example.lesson11;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Написать метод, который читает текстовый файл и возвращает его в виде списка строк.
 * Написать метод, который записывает в файл строку, переданную параметром.
 * Используя решение 1 и 2, напишите метод, который склеивает два текстовый файла один.
 * Написать метод для копирования файла (побайтно, или массивами байт).
 * Написать метод, который в каталоге ищет файлы, в имени которых содержится определенная строка, и который возвращает список имен таких файлов.
 * Написать метод, который в каталоге ищет текстовые файлы, в которых содержится определенная строка, и которая возвращает список имен таких файлов. (FilenameFilter)
 */

@Slf4j
public class Main {

    public static void main(String[] args) throws IOException {
        List<String> lines = Stream.of("line1", "line2", "line3", "line4").collect(Collectors.toList());
        String file1 = "./target/file1.txt";
        String file2 = "./target/file2.txt";
        appendLinesToFile(file1, lines);
        appendLinesToFile(file2, lines);

        // getLinesFromFile - читает текстовый файл и возвращает его в виде списка строк
        // appendLinesToFile - записывает в файл строку, переданную параметром
        // (если строка одна - используем Collections.singletonList("line1")
        appendLinesToFile(file2, getLinesFromFile(file1));

        String file3 = "./target/file3.txt";

        // copyFileByteByByte - метод для копирования файла побайтно
        copyFileByteByByte(file2, file3);

        // getFilesByWildcard - в каталоге ищет файлы, в имени которых содержится определенная строка
        log.info("Get files by wildcard: {}", getFilesByWildcard("./target/", "file*"));

        // метод, который в каталоге ищет текстовые файлы, в которых содержится определенная строка
        log.info("TXT files with \"line\" in them: {}",
                getFilesByWildcard("./target/", "*.txt").stream().filter(file -> isInFile("line", file)).collect(Collectors.toSet()));
    }

    private static List<String> getLinesFromFile(String filePath) {
        List<String> list = new ArrayList<>();
        File file = new File(filePath);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; ) {
                list.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static void appendLinesToFile(String filePath, List<String> lines) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, true))) {
            for (String line : lines) {
                writer.write(String.format("%s%n", line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyFileByteByByte(String sourcePath, String targetPath) throws IOException {
        File source = new File(sourcePath);
        File target = new File(targetPath);
        if (target.exists()) {
            log.info("Deleting {}: {}", targetPath, Files.deleteIfExists(Paths.get(targetPath)));
        }
        log.info("Creating {}: {}", targetPath, target.createNewFile());
        try (DataInputStream din = new DataInputStream(new FileInputStream(source));
             DataOutputStream dout = new DataOutputStream(new FileOutputStream(target))) {
            int c;
            while ((c = din.read()) != -1) {
                dout.write((byte) c);
            }
        }
    }

    private static List<String> getFilesByWildcard(String folderPath, String wildCard) {
        List<String> list = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(folderPath), wildCard)) {
            stream.forEach(path -> list.add(path.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static boolean isInFile(String str, String filePath) {
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null)
                if (readLine.contains(str)) {
                    return true;
                }
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
