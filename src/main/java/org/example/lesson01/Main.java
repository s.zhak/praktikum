package org.example.lesson01;

import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
public class Main {
    public static void main(String[] args) {
        do1();
        do2();
        do3();
        do4();
        do5();
        do6();
    }

    private static void do1() {
        Stream.of("Я", "хорошо", "знаю", "Java.")
                .forEach(log::info);
    }

    private static void do2() {
        log.info("{}", (46 + 10) * (10 / 3));
        log.info("{}", 29 * 4 * -15);
    }

    private static void do3() {
        int a = 10500;
        int result = a / 10 / 10;
        log.info("Result: {}", result);
    }

    private static void do4() {
        Double result = Stream.of(3.6, 4.1, 5.9)
                .reduce(0d, Double::sum);
        log.info("Result: {}", result);
    }

    private static void do5() {
        Scanner scanner = new Scanner(System.in);
        log.info("Enter three integers: ");
        IntStream.rangeClosed(1, 3)
                .mapToObj(i -> scanner.nextInt())
                .forEach(i -> log.info("{}", i));
    }

    private static void do6() {
        Scanner scanner = new Scanner(System.in);
        int b = scanner.nextInt();
        if (b > 100) {
            log.info("Выход за пределы диапазона");
            return;
        }
        log.info("{} - {}", b, b % 2 == 0 ? "четное" : "нечетное");
    }

}

/*
        1. Выведите строки в следующем порядке:
        Я
        хорошо
        знаю
        Java.
        2. Посчитайте результат выражения
        (46 + 10) * (10 / 3)
        (29) * (4) * (-15)
        3. В переменной number, лежит целое число 10500. В переменной result посчитайте следующее
        выражение: (number / 10) / 10. Результат выведите на консоль.
        4. Даны три числа: 3.6, 4.1, 5.9. В переменной result посчитайте произведение этих чисел.
        5. В этой задаче вы должны считать целые числа из стандартного ввода, а затем вывести. Каждое
        целое число нужно печатать с новой строки. Формат ввода:
        42
        100
        125
        6. Для целого числа b выполните следующие условные действия:
        ● Если b нечетное, выведите “Нечетное”
        ● Если b четное, выведите “Четное”
        ● Если b четное и больше 100, выведите “Выход за пределы диапазона”
        ! Использовать класс Scanner: int b = scanner.nextInt();
*/
