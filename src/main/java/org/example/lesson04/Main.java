package org.example.lesson04;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) {
        log.info("{}", do1(Arrays.asList("Java", "Kotlin", "Z")));

        log.info("{}", do2("Rose is not a palindrome"));

        log.info("{}", do3("А бяка упала на лапу Азора"));

        String s1 = "красный желтый синий желтый черный";
        String s2 = "желтый";
        log.info("[{}] встречается в [{}] ровно {} раз[а].", s2, s1, do4(s1, s2));

        log.info("{}", do5("каждый охотник желает знать где сидит фазан"));
    }

    private static String do1(List<String> lines) {
        return lines.stream()
                .max(Comparator.comparingInt(String::length))
                .orElse(null);
    }

    private static Boolean do2(String str) {
        StringBuilder builder = new StringBuilder(str);
        return builder.toString().equals(builder.reverse().toString());
    }

    private static String do3(String str) {
        return str.replace("бяка", "[РОСКОМНАДЗОР]");
    }

    private static int do4(String str, String substring) {
        Pattern pattern = Pattern.compile(substring);
        Matcher matcher = pattern.matcher(str);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    private static String do5(String str) {
        List<String> list = Arrays.asList(str.split(" "));
        Collections.reverse(list);
        return String.join(" ", list);
    }
}
