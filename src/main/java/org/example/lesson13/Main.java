package org.example.lesson13;

import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;
import java.util.stream.Stream;

@Slf4j
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        log.info("Enter request for Wikipedia:");
        String requestLine = scanner.nextLine();
        if (requestLine != null && requestLine.length() > 0) {
            // запрос введенного с клавиатуры запроса
            (new Worker(requestLine)).start();
        } else {
            log.info("You've entered empty request. Try asking for something!");
        }

        // асинхронный запрос кучи информации
        // результат поиска после вывода можно взять из объекта worker из поля answerList
        // чтобы быть уверенным в наличии результата - использовать worker.join
        Stream.of("java", "marvel", "dc", "kotlin", "guardians", "forest", "tree")
                .parallel()
                .map(Worker::new)
                .forEach(Worker::start);
    }
}
