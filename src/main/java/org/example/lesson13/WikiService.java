package org.example.lesson13;

import com.google.gson.JsonObject;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface WikiService {
    @RequestLine("GET /w/api.php?action=query&list=search&utf8=&format=json&srsearch=\"{searchTerm}\"")
    @Headers("Content-Type: applicaion/json")
    JsonObject searchByExactTerm(@Param("searchTerm") String searchTerm);

    @RequestLine("GET /w/api.php?action=query&list=search&utf8=&format=json&srsearch={searchTerm}")
    @Headers("Content-Type: applicaion/json")
    JsonObject searchByTerm(@Param("searchTerm") String searchTerm);
}
