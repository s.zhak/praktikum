package org.example.lesson13;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FeignConfig {

    private static WikiService wikiService;

    public static WikiService getWikiService() {
        if (wikiService == null) {
            wikiService = Feign.builder()
                    .client(new OkHttpClient())
                    .encoder(new GsonEncoder())
                    .decoder(new GsonDecoder())
                    .target(WikiService.class, "https://ru.wikipedia.org");
        }
        return wikiService;
    }
}
