package org.example.lesson13;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Slf4j
public class Worker extends Thread {

    private String requestLine;
    private List<String> answerList = new ArrayList<>();

    @Override
    public void run() {
        printSearchResults();
    }

    public Worker(String requestLine) {
        super();
        this.requestLine = requestLine;
    }

    public void printSearchResults() {
        WikiService wikiService = FeignConfig.getWikiService();
        JsonObject answer = wikiService.searchByExactTerm(requestLine);
        JsonObject query = answer.getAsJsonObject("query");
        JsonArray search = query.getAsJsonArray("search");
        answerList.clear();
        search.forEach(searchElement -> answerList.add(searchElement.getAsJsonObject().get("snippet").getAsString()));
        answerList.stream()
                .map(String::toString)
                .forEach(log::info);
    }
}
