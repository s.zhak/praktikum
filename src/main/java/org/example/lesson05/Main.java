package org.example.lesson05;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class Main {
    public static void main(String[] args) throws VectorException {
        Vector a = new Vector(3, new int[]{10, 15, 5});
        Vector b = new Vector(3, new int[]{-5, 10, -15});

        log.info("Длина вектора a: {}", a.getLength());
        log.info("Длина вектора b: {}", b.getLength());

        log.info("Скалярное произведение a на b: {}", a.scalarMultiply(b));
        log.info("Векторное произведение a на b: {}", a.vectorMultiply(b));
        log.info("Косинус угла между векторами a и b: {}", a.getCosinus(b));
        log.info("Сумма векторов a и b: {}", a.plus(b));
        log.info("Разность векторов a и b: {}", a.minus(b));

        log.info("Десять случайных векторов:");
        Arrays.stream(Vector.getNRandomVectors(3, 10))
                .forEach(v -> log.info("{}", v));
    }
}