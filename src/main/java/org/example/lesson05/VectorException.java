package org.example.lesson05;

public class VectorException extends Exception {
    public VectorException(String message) {
        super(message);
    }

    public VectorException(Throwable cause) {
        super(cause);
    }

    public VectorException(String message, Throwable cause) {
        super(message, cause);
    }
}
