package org.example.lesson05;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Random;

@Slf4j
@Data
@AllArgsConstructor
public class Vector {
    private int dimensions;
    private int[] coords;
    private static final Random random = new Random();
    private static final String ERROR_DIMENSIONS_MESSAGE = "Dimensions should be the same for both vectors!";


    public double getLength() {
        double tmp = Arrays.stream(coords)
                .mapToDouble(x -> x * x)
                .reduce(0d, Double::sum);
        return Math.sqrt(tmp);
    }

    public double scalarMultiply(Vector b) throws VectorException {
        if (this.dimensions != b.dimensions)
            throw new VectorException(ERROR_DIMENSIONS_MESSAGE);
        double tmp = 0d;
        for (int i = 0; i < this.dimensions; i++) {
            tmp += this.coords[i] * b.coords[i];
        }
        return tmp;
    }

    public Vector vectorMultiply(Vector b) throws VectorException {
        if (this.dimensions != b.dimensions)
            throw new VectorException(ERROR_DIMENSIONS_MESSAGE);
        //TODO: implement floating dimension vectors multiplying
        if (this.dimensions != 3) throw new VectorException("Implemented only for 3-dim vectors!");
        int[] newCoords = new int[3];
        newCoords[0] = this.coords[1] * b.coords[2] - this.coords[2] * b.coords[1];
        newCoords[1] = this.coords[2] * b.coords[0] - this.coords[0] * b.coords[2];
        newCoords[2] = this.coords[0] * b.coords[1] - this.coords[1] * b.coords[0];
        return new Vector(3, newCoords);
    }

    public double getCosinus(Vector b) throws VectorException {
        if (this.dimensions != b.dimensions)
            throw new VectorException(ERROR_DIMENSIONS_MESSAGE);
        return this.scalarMultiply(b) / Math.abs(this.getLength() * b.getLength());
    }

    public Vector plus(Vector b) throws VectorException {
        if (this.dimensions != b.dimensions)
            throw new VectorException(ERROR_DIMENSIONS_MESSAGE);
        int[] newCoords = new int[this.dimensions];
        for (int i = 0; i < this.dimensions; i++) {
            newCoords[i] = this.coords[i] + b.coords[i];
        }
        return new Vector(this.dimensions, newCoords);
    }

    public Vector minus(Vector b) throws VectorException {
        if (this.dimensions != b.dimensions)
            throw new VectorException(ERROR_DIMENSIONS_MESSAGE);
        int[] newCoords = new int[this.dimensions];
        for (int i = 0; i < this.dimensions; i++) {
            newCoords[i] = this.coords[i] - b.coords[i];
        }
        return new Vector(this.dimensions, newCoords);
    }

    public static Vector[] getNRandomVectors(int dimensions, int n) {
        Vector[] vectors = new Vector[n];
        for (int i = 0; i < n; i++) {
            int[] newCoords = new int[dimensions];
            for (int j = 0; j < dimensions; j++) {
                newCoords[j] = random.nextInt(100) - 50;
            }
            vectors[i] = new Vector(dimensions, newCoords);
        }
        return vectors;
    }
}
