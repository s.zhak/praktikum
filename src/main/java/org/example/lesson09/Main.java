package org.example.lesson09;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Random;

/**
 * Все методы должны иметь проверку параметров и генерировать исключения при
 * необходимости.
 * <p>
 * 1. Написать метод для поиска наибольшего элемента в двумерном массиве.
 * 2. Написать метод, который проверяет, является ли двумерный массив квадратным.
 * 3. Написать метод, который, в двумерном массиве (матрице) ищет строку, сумма
 * элементов которой является максимальной среди всех строк матрицы.
 * 4. Двумерный массив MxN заполнить случайными символами алфавита
 */

@Slf4j
public class Main {

    private static final String ERROR_MSG = "You have to have an array before doing that!";
    private static final Random random = new Random();

    public static void main(String[] args) {
        int[][] arr = new int[][]{{1, 2, 3, 4, 5}, {6, 7, 8, 9, 0}};
        log.info("do1: {}", getMax(arr));
        log.info("do2: {}", isSquare(arr));
        log.info("do3: {}", getMaxLine(arr));
        log.info("do4: {}", Arrays.deepToString(fillMatrixWithChars(4, 5)));

        int[] do51 = new int[]{1, 2, 3, 4, 5, 6, 7};
        int[] do52 = new int[]{4, 5, 6, 7, 8, 9, 0};
        log.info("do5: {}", getRandomDigitFromArrayThatExistsInSecondArray(do51, do52));
    }

    public static int getMax(int[][] arr) {
        if (arr == null || arr.length == 0) {
            throw new Lesson09Exception(ERROR_MSG);
        }
        return Arrays.stream(arr).mapToInt(Main::getMax).max().orElse(0);
    }

    public static int getMax(int[] arr) {
        if (arr == null || arr.length == 0) {
            throw new Lesson09Exception(ERROR_MSG);
        }
        return Arrays.stream(arr).max().orElse(0);
    }

    public static Boolean isSquare(int[][] arr) {
        if (arr == null || arr.length == 0) {
            throw new Lesson09Exception(ERROR_MSG);
        }
        int len1 = arr.length;
        int len2 = arr[0].length;
        for (int[] ints : arr) {
            if (ints.length != len2) {
                return Boolean.FALSE;
            }
        }
        return len1 == len2;
    }

    public static int getMaxLine(int[][] arr) {
        if (arr == null || arr.length == 0) {
            throw new Lesson09Exception(ERROR_MSG);
        }
        return Arrays.stream(arr).mapToInt(Main::getLineSum).max().orElse(0);
    }

    public static int getLineSum(int[] arr) {
        if (arr == null || arr.length == 0) {
            throw new Lesson09Exception(ERROR_MSG);
        }
        return Arrays.stream(arr).sum();
    }

    public static char[][] fillMatrixWithChars(int m, int n) {
        char[][] arr = new char[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = getRandomChar();
            }
        }
        return arr;
    }

    public static char getRandomChar() {
        return (char) (random.nextInt(26) + 'a');
    }

    /**
     * 5. Дан массив чисел numbers, и дан массив weight такой же длины.
     * Задача: написать метод, который бы случайно выбирал число из первого массива, которое есть во втором массиве.
     */
    public static int getRandomDigitFromArrayThatExistsInSecondArray(int[] numbers, int[] weight) {
        return Arrays.stream(numbers).filter(i -> numberExistsInArray(i, weight)).findAny().orElseThrow(() -> new Lesson09Exception("Don't have any"));
    }

    public static boolean numberExistsInArray(int number, int[] array) {
        return Arrays.stream(array).anyMatch(i -> i == number);
    }
}

class Lesson09Exception extends RuntimeException {
    public Lesson09Exception(String message) {
        super(message);
    }

    public Lesson09Exception(String message, Throwable cause) {
        super(message, cause);
    }
}