package org.example.lesson08;

import lombok.ToString;

/**
 * наследование класса Человек;
 * реализация функции для вывода информации;
 * строковое поле «название банка»;
 * конструктор для установки всех значений;
 */

@ToString(callSuper = true)
public class Client extends Human {
    private final String bankName;

    public Client(String firstName, String lastName, String bankName) {
        super(firstName, lastName);
        this.bankName = bankName;
    }
}
