package org.example.lesson08;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * поля: имя, фамилия;
 * метод для получение имени;
 * метод для получение фамилии;
 * абстрактный метод для вывода всей информации;
 * конструктор для установки значений.
 */

@Getter
@ToString
@AllArgsConstructor
abstract class Human {
    private final String firstName;
    private final String lastName;
}
