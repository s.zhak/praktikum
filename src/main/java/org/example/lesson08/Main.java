package org.example.lesson08;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

/**
 * Создайте несколько классов:
 * абстрактный класс Человек;
 * класс Клиент;
 * класс Работник банка
 */

@Slf4j
public class Main {
    public static void main(String[] args) {
        ArrayList<Human> humans = new ArrayList<>();
        humans.add(new Employee("Василий", "Пупкин", "Сбербанк"));
        humans.add(new Client("Мария", "Петрова", "Сбербанк"));
        humans.stream()
                .map(Human::toString)
                .forEach(log::info);
    }
}
