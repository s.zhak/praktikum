package org.example.lesson03;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;

@Slf4j
public class MergeSort {

    private static final Random random = new Random();
    private static final int BOUND = 20;

    public static void main(String[] args) {
        int[] array = new int[BOUND];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100)-50;
        }
        log.info("Got array: {}", array);
        mergeSort(array, 0, array.length-1);
        log.info("Sorted:    {}", array);
    }

    public static void mergeSort(int[] array, int left, int right) {
        if (right <= left) return;
        int mid = (left + right) / 2;
        mergeSort(array, left, mid);
        mergeSort(array, mid + 1, right);
        merge(array, left, mid, right);
    }

    private static void merge(int[] array, int left, int mid, int right) {
        // get R and L lengths
        int lengthLeft = mid - left + 1;
        int lengthRight = right - mid;

        // temp arrays
        int leftArray[] = new int[lengthLeft];
        int rightArray[] = new int[lengthRight];

        // fill temp arrays
        for (int i = 0; i < lengthLeft; i++)
            leftArray[i] = array[left + i];
        for (int i = 0; i < lengthRight; i++)
            rightArray[i] = array[mid + i + 1];

        // iterators
        int leftIndex = 0;
        int rightIndex = 0;

        // get from L and R to main array
        for (int i = left; i < right + 1; i++) {
            // search for minimum elements
            if (leftIndex < lengthLeft && rightIndex < lengthRight) {
                if (leftArray[leftIndex] < rightArray[rightIndex]) {
                    array[i] = leftArray[leftIndex];
                    leftIndex++;
                } else {
                    array[i] = rightArray[rightIndex];
                    rightIndex++;
                }
            }
            // if R is empty
            else if (leftIndex < lengthLeft) {
                array[i] = leftArray[leftIndex];
                leftIndex++;
            }
            // if L is empty
            else if (rightIndex < lengthRight) {
                array[i] = rightArray[rightIndex];
                rightIndex++;
            }
        }
    }
}
