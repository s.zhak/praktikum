package org.example.lesson03;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class Main {

    private static final Random random = new Random();

    public static void main(String[] args) throws Exception {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (random.nextInt(10) - 5);
        }
        log.info("Got array: {}", array);
        do1(array);
        do3(array);
        log.info("Got reversed: {}", array);
        log.info("First unique: {}", do4(array));
        log.info("Number 4: {}", do5(4));
        log.info("Sorted: {}", do6(array));
        log.info("do7: {}", do7(array, 4));
    }

    private static void do1(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                log.info("Try again! It's not sorted!");
                return;
            }
        }
        log.info("OK");
    }

    private static void do2() {
        Scanner scanner = new Scanner(System.in);
        int bnd = scanner.nextInt();
        int[] arr = new int[bnd];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        log.info("Got array: {}", arr);
    }

    private static void do3(int[] array) {
        int tmp = array[0];
        array[0] = array[array.length - 1];
        array[array.length - 1] = tmp;
    }

    private static int do4(int[] array) throws Exception {
        for (int i = 0; i < array.length; i++) {
            int doubles = 0;
            for (int j = i; j < array.length; j++) {
                if (array[i] == array[j]) doubles++;
            }
            if (doubles == 1) return array[i];
        }
        throw new Exception("No unique values!");
    }

    private static int do5(int num) {
        if (num <= 2) return 1;
        int a = 1;
        int b = 1;
        for (int i = 0; i < num - 2; i++) {
            a = a + b;
            b = a - b;
        }
        return a;
    }

    private static int[] do6(int[] array) {
        MergeSort.mergeSort(array, 0, array.length - 1);
        return array;
    }

    private static int[] do7(int[] array, int k) {

        MergeSort.mergeSort(array, 0, array.length - 1);
        Map<Integer, Integer> counters = new HashMap<>();

        for (int el : array) counters.put(el, counters.get(el) == null ? 1 : counters.get(el) + 1);

        int[] counterValues = counters.values().stream().mapToInt(v -> v).toArray();
        MergeSort.mergeSort(counterValues, 0, counterValues.length - 1);

        ArrayList<Integer> used = new ArrayList<>();
        AtomicInteger cnt = new AtomicInteger(1);

        while (cnt.get() <= k) {
            int frequency = counterValues[counterValues.length - cnt.get()];
            counters.forEach((key, value) -> {
                if (value == frequency && !used.contains(key) && cnt.get() <= k) {
                    log.info("{} is in array {} times", key, value);
                    used.add(key);
                    cnt.getAndIncrement();
                }
            });
        }
        return used.stream().mapToInt(v -> v).toArray();
    }
}
