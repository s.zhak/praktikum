package org.example.lesson06;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
class Study {

    private String course;

    public String printCourse() {
        return this.course;
    }
}