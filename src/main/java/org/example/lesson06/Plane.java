package org.example.lesson06;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@Slf4j
@Builder
public class Plane {
    private Wing leftWing;
    private Wing rightWing;
}

@Data
@AllArgsConstructor
@Slf4j
@Builder
class Wing {
    private int weight;
}
