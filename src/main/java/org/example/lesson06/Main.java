package org.example.lesson06;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class Main {
    public static void main(String[] args) {
        do1();
        do2();
        do3();
        do4();
    }

    private static void do1() {
        Optional.of("Изучение Java - это просто!")
                .map(Study::new)
                .map(Study::printCourse)
                .ifPresent(log::info);
    }

    private static void do2() {
        List<House> houses = new ArrayList<>();

        houses.add(
                Optional.of(new House())
                        .map(h -> {
                            h.setStories(1);
                            return h;
                        })
                        .map(h -> {
                            h.setYear(1948);
                            return h;
                        })
                        .map(h -> {
                            h.setName("Барак");
                            return h;
                        })
                        .orElse(null)
        );

        houses.add(new House(5, 1991, "Пятиэтажка"));
        houses.add(House.builder()
                .stories(9)
                .year(2014)
                .name("Девятиэтажка")
                .build());
        houses.stream()
                .map(House::toString)
                .forEach(log::info);
    }

    private static void do3() {
        List<Tree> trees = new ArrayList<>();
        trees.add(new Tree(120, true, "секвойя"));
        trees.add(new Tree(20, "клён"));
        trees.add(new Tree());
        trees.stream()
                .map(Tree::toString)
                .forEach(log::info);
    }

    private static void do4() {
        List<Plane> planes = new ArrayList<>();
        planes.add(new Plane(new Wing(10), new Wing(20)));
        planes.add(Plane.builder()
                .leftWing(Wing.builder().weight(15).build())
                .rightWing(Wing.builder().weight(25).build())
                .build());
        planes.stream()
                .map(Plane::toString)
                .forEach(log::info);
    }
}

/*1. Допишите в класс «Study» конструктор, что будет принимать один параметр и будет устанавливать
значение этого параметра в поле «course». Создайте объект класса «Study». В качестве значения для
поля «course» установите текст: «Изучение Java - это просто!». Обратитесь к методу «printCourse»
для вывода значения поля «course».

class Study {

   private String course;

   //TODO

   public String printCourse() {
       return this.course;
   }
}

class JavaProgram {
   public static void main(String[] args) {
   //TODO
   }
}

2. Создайте класс Дом. Добавьте в него следующие данные:
поля: количество этажей, год постройки, наименование;
метод для установки всех значений;
метод для вывода всех значений;
метод, возвращающий количество лет с момента постройки.

На основе класса создайте два объекта и пропишите для каждого характеристики. Добавление
характеристик реализуйте через метод класса.

Выведите информацию про каждый объект.


3. Создайте класс Tree. Добавьте в него поля: возраст, живое ли дерево и название
дерева. Создайте три конструктора:
Конструктор, который устанавливает только возраст и название;
Конструктор, который устанавливает все переменные в классе;
Конструктор, который ничего не устанавливает, но выводит сообщение «Пустой
конструктор без параметров сработал».

Создайте три объекта на основе класса и используйте по одному конструктору на каждый.

4. Создайте класс Самолет. В классе создайте вложенный класс - крыло.
Внутри класса Крыло, реализуйте:
поле для хранения информации про вес крыла;
метод, который будет показывать вес крыла.

В главном классе создайте объект и добавьте во вложенный класс данные
про вес крыла и выведите информацию на экран. Создайте два объекта и
добавьте разный вес для крыльев.
*/