package org.example.lesson06;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Data
@Slf4j
public class Tree {
    private int age;
    private boolean live;
    private String name;

    public Tree(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public Tree() {
        log.info("Пустой конструктор без параметров сработал.");
    }
}
