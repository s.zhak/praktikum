package org.example.lesson12;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogMyName extends Thread {
    @Override
    public void run() {
        log.info("My name is: {}", this.getName());
    }
}
