package org.example.lesson12;

import java.util.concurrent.atomic.AtomicInteger;

public class FixedCounter {
    AtomicInteger count = new AtomicInteger(0);
    public void increment() {
        count.getAndIncrement();
    }
    public int getCount() {
        return count.get();
    }
}