package org.example.lesson12;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Worker extends Thread {

    @Override
    public void run() {
        log.info("While running: {}", this.getState());
    }
}
