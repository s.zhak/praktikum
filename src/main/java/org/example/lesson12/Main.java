package org.example.lesson12;

import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.stream.IntStream;

@Slf4j
public class Main {
    public static void main(String[] args) {
        /*
         * Напишите программу, в которой запускается 10 потоков и каждый из них выводит
         * числа от 0 до 100.
         */
        IntStream.rangeClosed(1, 10)
                .parallel()
                .forEach(i -> {
                    for (int j = 0; j < 100; j++) {
                        log.info("{}", j);
                    }
                });


        /*
         * Выведите состояние потока перед его запуском, после запуска и во время
         * выполнения.
         */
        Optional.of(new Worker())
                .ifPresent(worker -> {
                    log.info("Before start: {}", worker.getState());
                    worker.start();
                    try {
                        worker.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Thread.currentThread().interrupt();
                    }
                    log.info("After start: {}", worker.getState());
                });


        /*
         * Напишите программу, в которой запускается 100 потоков, каждый из которых
         * вызывает метод increment() 1000 раз.
         * После того, как потоки завершат работу, проверьте, чему равен count .
         * Если обнаружилась проблема, предложите ее решение.
         */
        Counter counter = new Counter();
        IntStream.rangeClosed(1, 100)
                .parallel()
                .forEach(i -> {
                    for (int j = 0; j < 1000; j++) {
                        counter.increment();
                    }
                });
        log.info("Count: {}", counter.getCount());
        // count насчитался меньше из-за неверного конкурентного доступа.
        // решение - в классе FixedCounter


        /*
         * Напишите программу, в которой создаются два потока, каждый из которых выводит
         * по очереди на консоль своё имя.
         */
        IntStream.rangeClosed(1, 2)
                .parallel()
                .mapToObj(i -> new LogMyName())
                .forEach(LogMyName::run);
    }
}
